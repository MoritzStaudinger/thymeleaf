package hajszan;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;

@ComponentScan({"hajszan"})
public interface UserService extends PagingAndSortingRepository<User, Integer>{
	public List<User> findAll();
}

	