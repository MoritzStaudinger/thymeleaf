package hajszan;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Service("userService")
@Repository
@ComponentScan({"hajszan"})
public class UserServiceImpl implements  UserService {

	/*
	 * ERROR ERROR ERROR - - PLEASE HELP - - ERROR ERROR ERROR 
	 */
	//@Autowired <-- This Autowired Annotation is not working properly. I have tried for several hours to fix this, but I just cant find the error in my programm
	@Qualifier("userRepo")
	private UserRepository userRepository;
	
	
	public List<User> findAll() {
		return userRepository.findAll();
	}


	@Override
	public Iterable<User> findAll(Sort arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Page<User> findAll(Pageable arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void delete(Integer arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void delete(User arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void delete(Iterable<? extends User> arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean exists(Integer arg0) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public Iterable<User> findAll(Iterable<Integer> arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public User findOne(Integer arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <S extends User> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <S extends User> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
