package hajszan;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class User  {
	@Override
	public String toString() {
		return "User [id=" + id + ", firstname=" + firstname + ", lastname="
				+ lastname + "]";
	}

	// AbstractPersistable ... which has an auto-increment (depends on DB) primary key 
	// field in it and some utility methods. 
	private static final long serialVersionUID = 1L;
	@Id
	private int id;
	private String firstname;
	private String lastname;
	
	private User( ) {}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
	