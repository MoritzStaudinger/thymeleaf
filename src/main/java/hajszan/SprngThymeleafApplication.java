package hajszan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"hajszan"})
@EnableAutoConfiguration
@SpringBootApplication
public class SprngThymeleafApplication {

	
    public static void main(String[] args) {
        SpringApplication.run(SprngThymeleafApplication.class, args);
    }

}
