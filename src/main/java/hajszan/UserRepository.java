package hajszan;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepo")
@ComponentScan({"hajszan"})
public interface UserRepository {
	
	public List<User> findAll();
}

	 